Feature: User Push feature

  Scenario: As a User, I should be able to push a value into the Stack.
     When I enter "1" into the input field
     And I press "Push"
     Then I should see "Pushed 1"

  Scenario: As a User, I should be able to push a value until it is full.
     When I enter "1" into the input field
     And I press "Push"
     Then I should see "Pushed 1"
     When I enter "2" into the input field
     And I press "Push"
     Then I should see "Pushed 2"
     When I enter "3" into the input field
     And I press "Push"
     Then I should see "Pushed 3"
     When I enter "4" into the input field
     And I press "Push"
     Then I should see "Full"