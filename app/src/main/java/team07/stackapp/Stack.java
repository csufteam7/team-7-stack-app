package team07.stackapp;//hello john

/**
 * Created by Noemi Quezada on 9/18/15.
 */
public class Stack {
    private static int STACK_SIZE = 3; // Size of the stack
    int[] theStack; // The Stack
    int top = -1; // Top of the Stack

    // This is the constructor of the Stack Class
    public Stack(){
        theStack = new int[STACK_SIZE];
    }

    // Push a Value into the Stack
    public boolean push(int value) {
        if (top < STACK_SIZE-1) { // If the top of the Stack is smaller than the Size of the Stack
            top++;
            theStack[top] = value; // Add new value
            return true;
        }
        return false;
    }

    // Pop the Top of the Stack
    public int pop() {
        if (top > -1){ // If the top of the stack is not -1
            int poppedInt = theStack[top];
            top--; // Remove an element
            return poppedInt;
        }
        return -1;
    }

    // Check if Stack is Empty
    public boolean isEmpty(){
        if (top == -1){
            return true;
        }
        return false;
    }

    public int getTop()
    {
        if (isEmpty())
        {
            return -1;
        }
        return theStack[top];
    }

    public String getStack(){
        String outStack = "";
        int index = -1;
        while (index < top) {
            index++;
            if (index == top){
                outStack = outStack + theStack[index];
            }
            else {
                outStack = outStack + theStack[index] + ", ";
            }
        }
        return outStack;
    }
}
