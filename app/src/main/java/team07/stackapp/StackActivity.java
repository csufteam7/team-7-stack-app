package team07.stackapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import team07.stackapp.Stack; //Import the Stack Class

public class StackActivity extends AppCompatActivity {

    Stack theStack = new Stack();
    Button pushButton;
    EditText inputValueEditText;
    TextView message;
    TextView stackView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stack);

        //Set the Push Button to be disabled from the start
        pushButton = (Button) findViewById(R.id.push_button);
        inputValueEditText = (EditText) findViewById(R.id.value_input);
        message = (TextView)findViewById(R.id.message);
        stackView = (TextView) findViewById(R.id.stackOutput);

        pushButton.setEnabled(false); // set Push Button disable initially

        //Enable Button if the Field is not empty
        inputValueEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

                if (s.toString().equals("")) {
                    pushButton.setEnabled(false);
                } else {
                    pushButton.setEnabled(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        //Dismiss the Keyboard when it changes focus
        inputValueEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stack, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** Called when the user clicks the Push button */
    public void pushValue(View view) {
        //Get the Users input
        inputValueEditText = (EditText)findViewById(R.id.value_input);

        String inputValueString = inputValueEditText.getText().toString();

        // If the inputValueString is not empty
        if ( inputValueString != ""){

            // Turn Value into String
            int integerValue = Integer.parseInt(inputValueString);

            if (theStack.push(integerValue)){
                message = (TextView)findViewById(R.id.message);
                message.setText("Pushed " + inputValueString);
                inputValueEditText.setText("");
            }
            else{
                message.setText("Full Stack");
                inputValueEditText.setText("");
            }
        } else {
            ;
            message.setText("Blank");
        }
        stackView.setText(theStack.getStack());
        hideKeyboard(inputValueEditText);
    }

    /** Called when the user clicks the Pop Button */
    public void popValue(View view){
        if (!theStack.isEmpty()){
            message.setText("Popped " + theStack.getTop());
            theStack.pop();
        }else{
            message.setText("Empty Stack");
        }
        stackView.setText(theStack.getStack());
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void exitButton(View view){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }





}
