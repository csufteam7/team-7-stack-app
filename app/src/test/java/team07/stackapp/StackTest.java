package team07.stackapp;

import junit.framework.TestCase;

/**
 * Created by martin on 9/21/2015.
 */
public class StackTest extends TestCase {
    protected Stack myStack;

    protected void setUp()throws Exception{

        myStack = new Stack();
    }

    public void testPush(){
        boolean result = myStack.push(5);
        assertTrue(result == true);
    }

    public void testPop() {
        myStack.push(5);
        int result = myStack.pop();
        assertTrue(result == 5);
    }

    public void testIsEmpty1(){
        myStack.push(5);
        myStack.push(4);
        myStack.push(3);
        boolean result = myStack.isEmpty();
        assertTrue(result == false);
    }

    public void testIsEmpty(){
        myStack.push(5);
        myStack.push(4);
        myStack.push(3);
        myStack.pop();
        myStack.pop();
        myStack.pop();
        boolean result = myStack.isEmpty();
        assertTrue(result == true);
    }

    public void testTop() {
        myStack.push(5);
        myStack.push(4);
        myStack.push(3);
        int result = myStack.getTop();
        assertTrue(result == 3);
    }

    public void testGetStack() {
        myStack.push(3);
        myStack.push(5);
        myStack.push(2);
        String result = myStack.getStack();
        assertTrue(result.equals("3, 5, 2"));
    }
}
